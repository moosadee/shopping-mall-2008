#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <allegro.h>
#include "rjm_bodyPartClass.h"
#include "rjm_buttonClass.h"
#include "rjm_gridClass.h"
#include "rjm_mapClass.h"
#include "rjm_playerClass.h"
#include "rjm_npcClass.h"
#include "common.h"
using namespace std;

volatile long speed_counter = 0;
void increment_speed_counter();
void init_allegro(bool);
void init_buttons(buttonClass [btn_amt]);
void check_click(buttonClass button, int mouse_x, int mouse_y, int index, bool *done, bool *, bool *, MIDI*);
int check_movement(rjm_playerClass[2], rjm_mapClass, bool two_player, BITMAP*);
void go_to_map(rjm_mapClass[], rjm_playerClass[], int, int*, bool*);
void move_player( rjm_playerClass*, rjm_mapClass, string, float, SAMPLE*,  rjm_npcClass clerk[6], int map_index );
void init_clerks( rjm_npcClass[] );
void check_clerk_collision( rjm_playerClass player[2], rjm_npcClass clerk[6], int map_index );

int main(int argc, char *argv[])
{
    bool fullscreen = true;
    srand(0);
    init_allegro(fullscreen);
    BITMAP *buffer = create_bitmap(scr_w*2, scr_h*2);
    BITMAP *graphics = load_bitmap("shoppingGraphics.bmp", NULL);
    BITMAP *tileset = load_bitmap("shoppingTileset.bmp", NULL);
    BITMAP *clerkimg = load_bitmap("shopping_clerks.bmp", NULL);
    MIDI *song[6];
    SAMPLE *sfx[1];
    song[0] = load_midi("shopping_menu_FF1airsh.mid");            //menu
    song[1] = load_midi("shopping_mall_HMmenu.mid");              //mall
    song[2] = load_midi("shopping_clothes_SSBM_dr_mario.mid");    //clothes
    song[3] = load_midi("shopping_hairdo_HMegg.mid");             //hair
    song[4] = load_midi("shopping_makeup_MK.mid");                //makeup
    song[5] = load_midi("shopping_shoes_bennyhill.mid");          //shoes
    sfx[0] = load_sample("shopping_sfx1.wav");
    rjm_npcClass clerk[6];
    init_clerks(clerk);
    //rjm_mapClass map_mallt, map_mallb, map_clothes, map_hair, map_makeup, map_shoes, map;
    rjm_mapClass map[6];
    int map_index = 0;
    rjm_playerClass player[2];
    map[0].load_map("mall_bottom");
    map[1].load_map("mall_top");
    map[2].load_map("clothes");
    map[3].load_map("hair");
    map[4].load_map("makeup");
    map[5].load_map("shoes");
    bool done = false;
    bool menu = true;
    bool two_player = true;
    buttonClass button[btn_amt];
    bool click = false;
    init_buttons(button);
    float game_anim = 0.0;
    float counter = 0.0;
    play_midi(song[0], true);
    player[1].init_p2();
    while (!done)
    {
        while (speed_counter > 0)
        {
            if (key[KEY_F4]) { done = true; }
            if (key[KEY_F5])
            {
                if ( fullscreen ) { set_gfx_mode(GFX_AUTODETECT_WINDOWED, scr_w, scr_h, 0, 0); fullscreen = false; }
                else { set_gfx_mode(GFX_AUTODETECT, scr_w, scr_h, 0, 0); fullscreen = true; }
            }
            if ( menu )
            {
                if ( mouse_b )
                {
                    click = true;
                }
                else
                {
                    if ( click == true )
                    {
                        //Check for button click
                        for ( int i=0; i<btn_amt; i++)
                        {
                            check_click(button[i], mouse_x, mouse_y, i, &done, &two_player, &menu, song[1]);
                            map_index = 0;
                        }
                        click = false;
                    }
                }
            }
            if ( !menu )
            {
                if (key[KEY_ESC])
                {
                    play_midi(song[0], true); menu = true;
                    player[0].x = 9*32;
                    player[0].y = 12*32;
                    player[1].x = 9*32+64;
                    player[1].y = 12*32;
                    player[0].force_update();
                    player[1].force_update();
                }
                if ( two_player == true )
                {
                    if ( key[KEY_8_PAD] )
                    {
                        move_player( &player[1], map[map_index], "up", counter, sfx[0], clerk, map_index );
                    }
                    else if ( key[KEY_2_PAD] )
                    {
                        move_player( &player[1], map[map_index], "down", counter, sfx[0], clerk, map_index );
                    }
                    if ( key[KEY_4_PAD] )
                    {
                        move_player( &player[1], map[map_index], "left", counter, sfx[0], clerk, map_index );
                    }
                    else if ( key[KEY_6_PAD] )
                    {
                        move_player( &player[1], map[map_index], "right", counter, sfx[0], clerk, map_index );
                    }
                    if ( key[KEY_8_PAD] || key[KEY_2_PAD] || key[KEY_4_PAD] || key[KEY_6_PAD] )
                    {
                        player[1].increment_frame();
                        int move = check_movement( player, map[map_index], two_player, buffer );
                        check_clerk_collision( player, clerk, map_index );
                        if ( move != -1 )
                        {
                            go_to_map( map, player, move, &map_index, &menu );
                            if ( map_index == 0 ) { play_midi(song[map_index+1], true); }
                            else { play_midi(song[map_index], true); }
                        }
                    }
                }
                //if ( key[KEY_ENTER] ) { player[0].swap('c'); }
                if ( key[KEY_UP] )
                {
//                    player[0].move("up");
                    move_player( &player[0], map[map_index], "up", counter, sfx[0], clerk, map_index );
                }
                else if ( key[KEY_DOWN] )
                {
//                    player[0].move("down");
                    move_player( &player[0], map[map_index], "down", counter, sfx[0], clerk, map_index );
                }
                if ( key[KEY_LEFT] )
                {
//                    player[0].move("left");
                    move_player( &player[0], map[map_index], "left", counter, sfx[0], clerk, map_index );
                }
                else if ( key[KEY_RIGHT] )
                {
//                    player[0].move("right");
                    move_player( &player[0], map[map_index], "right", counter, sfx[0], clerk, map_index );
                }
                if ( key[KEY_UP] || key[KEY_DOWN] || key[KEY_LEFT] || key[KEY_RIGHT] )
                {
                    player[0].increment_frame();
                    int move = check_movement( player, map[map_index], two_player, buffer );
                    check_clerk_collision( player, clerk, map_index );
                    if ( move != -1 )
                    {
                        int old = map_index;
                        go_to_map( map, player, move, &map_index, &menu );
                        if ( (old == 0 && map_index == 1) || (old == 1 && map_index == 0 ) ) { ; }
                        else if ( map_index == 0 ) { play_midi(song[1], true); }
                        else { play_midi(song[map_index], true); }
                    }
                }
            }
            counter += 0.1;
            game_anim += 0.1;
            if ( game_anim > 2.0 ) { game_anim = 0.0; }
            if ( counter > 5.0 ) { counter = 0.0; }
            for (int i=0; i<6; i++)
            {
                if ( clerk[i].talking ) { clerk[i].increment_counter(); }
            }
            speed_counter--;
        }//while (speed_counter > 0)
        vsync();
        if ( menu )
        {
            masked_blit(graphics, buffer, 0, 0, 0, 0, scr_w, scr_h);
            //draw buttons
            for (int i=0; i<btn_amt; i++)
            {
                masked_blit(graphics, buffer, button[i].grid_x, button[i].grid_y, button[i].x, button[i].y, button[i].w, button[i].h);
            }
            //Draw cursor
            masked_blit(graphics, buffer, 550 + (int)game_anim*30, 600, mouse_x, mouse_y, 30, 30);
        }
        else    //in-game
        {
            //draw map
            map[map_index].draw(buffer, tileset, game_anim);
            //draw player
            for (int i=0; i<6; i++)
            {
                if ( clerk[i].map == map_index )
                    clerk[i].draw(buffer, clerkimg);
            }
            player[0].draw(buffer);
            //draw player two
            if ( two_player ) { player[1].draw(buffer); }
            //textprintf(buffer, font, 0, 5, makecol(255, 255, 255), "(%i, %i)", player[0].x, player[0].y);
            map[map_index].draw_top(buffer, tileset, game_anim);
//            for (int i=0; i<max_x; i++)
//            {
//                for (int j=0; j<max_y; j++)
//                {
//                    textprintf(buffer, font, i*32, j*32, makecol(0, 0, 255), "%i", map[map_index].top_layer[i][j].codex / 32);
//                }
//            }
        }
        acquire_screen();
        blit(buffer, screen, 0, 0, 0, 0, scr_w, scr_h);
        clear_bitmap(buffer);
        release_screen();
    }//while (!done)
    return 0;
}
END_OF_MAIN();

void check_click(buttonClass button, int mouse_x, int mouse_y, int index, bool *done, bool *two_player, bool *menu, MIDI *mall_song)
{
    int left1 = mouse_x, left2, right1 = mouse_x + 40, right2, top1 = mouse_y, top2, bottom1 = mouse_y + 40, bottom2;
    left2 = button.x;
    right2 = left2 + button.w;
    top2 = button.y;
    bottom2 = top2 + button.h;
    if (    (bottom2 > top1) && (top2 < bottom1)    &&
            (right2 > left1) && (left2 < right1))
    {
        //clicked button
        if ( index == 0 )
        {
            (*menu) = false;
            (*two_player) = false;
            play_midi(mall_song, true);
        }
        else if ( index == 1 )
        {
            (*two_player) = true;
            (*menu) = false;
            play_midi(mall_song, true);
        }
        else if ( index == 2 )
        {
            (*done) = true;
        }
    }
}

void init_allegro(bool fullscreen)
{
    allegro_init();
    install_keyboard();
    install_timer();
    install_mouse();
    install_sound(DIGI_AUTODETECT, MIDI_AUTODETECT, 0);
    LOCK_VARIABLE(speed_counter);
    LOCK_FUNCTION(increment_speed_counter);
    install_int_ex(increment_speed_counter,BPS_TO_TIMER(90));
    set_color_depth(16);
    if ( fullscreen ) { set_gfx_mode(GFX_AUTODETECT, scr_w, scr_h, 0, 0); }
    else { set_gfx_mode(GFX_AUTODETECT_WINDOWED, scr_w, scr_h, 0, 0); }
    text_mode(-1);
}

void increment_speed_counter()
{
    speed_counter++;
}

void init_buttons(buttonClass button[btn_amt])
{
    button[0].x = 315;
    button[0].y = 400;
    button[0].w = 100;
    button[0].h = 100;
    button[0].grid_x = 50;
    button[0].grid_y = 600;
    button[0].go_to = "oneplayer";

    button[1].x = 435;
    button[1].y = 400;
    button[1].w = 100;
    button[1].h = 100;
    button[1].grid_x = 150;
    button[1].grid_y = 600;
    button[1].go_to = "twoplayer";

    button[2].x = 400;
    button[2].y = 525;
    button[2].w = 50;
    button[2].h = 50;
    button[2].grid_x = 0;
    button[2].grid_y = 600;
    button[2].go_to = "exit";
}


int check_movement(rjm_playerClass player[2], rjm_mapClass map, bool two_player, BITMAP *buffer)
{
    //This is for "warp" collision -- the doorways, basically, to get to another map
    // bottom2 > top1, top2 < bottom1
    // right2 > left1, left2 < right1
    for (int i=0; i<4; i++)
    {
        for (int j=0; j<2; j++)
        {
            if (    ( player[j].x + player[0].w     >       map.warp_x1[i] ) &&
                    ( player[j].x                   <       map.warp_x2[i] ) &&
                    ( player[j].y + player[0].h     >       map.warp_y1[i] ) &&
                    ( player[j].y + player[0].h/2   <       map.warp_y2[i] ) )
            {
                return i;
            }
        }
    }
    return -1;
}

void check_clerk_collision( rjm_playerClass player[2], rjm_npcClass clerk[6], int map_index )
{
    // right2 > left1, left2 < right1
    // bottom2 > top1, top2 < bottom1
    for (int i=0; i<6; i++)
    {
        if ( clerk[i].map == map_index )
        {
            for (int j=0; j<2; j++)
            {
                if (    ( player[j].x + player[j].w     >       clerk[i].talkx ) &&
                        ( player[j].x                   <       clerk[i].talkx + 32 ) &&
                        ( player[j].y + player[j].h     >       clerk[i].talky ) &&
                        ( player[j].y + player[j].h/2   <       clerk[i].talky + 32 ) )
                {
                    //collision
                    if ( clerk[i].talking == false )
                    {
                        int random = (int)rand()%2;
                        play_sample( clerk[i].speak[random], 255, 128, 1000, false ); clerk[i].talking = true;
                        play_sample( clerk[i].speak[random], 255, 128, 1000, false ); clerk[i].talking = true;
                    }
                }
            }
        }
    }
}

void go_to_map( rjm_mapClass map[6], rjm_playerClass player[2], int warp_index, int *map_index, bool *menu )
{
    int oldmap = *map_index;
    int newmap = map[oldmap].go_to[warp_index];
    if ( newmap == -1 )
    {
        newmap = 0;
        *map_index = newmap;
        player[0].x = map[newmap].enter_x[oldmap];
        player[0].y = map[newmap].enter_y[oldmap];
        player[1].x = map[newmap].enter_x[oldmap] + 64;
        player[1].y = map[newmap].enter_y[oldmap];
        player[0].force_update();
        player[1].force_update();
        *menu = true;
    }
    *map_index = newmap;
    player[0].x = map[newmap].enter_x[oldmap];
    player[0].y = map[newmap].enter_y[oldmap];
    player[1].x = map[newmap].enter_x[oldmap] + 64;
    player[1].y = map[newmap].enter_y[oldmap];
    player[0].force_update();
    player[1].force_update();
}


void move_player( rjm_playerClass *player, rjm_mapClass map, string direction, float temp_count, SAMPLE *sfx, rjm_npcClass clerk[6], int map_index )
{
    bool collide = false;
    bool hair = false;
    bool makeup = false;
    bool shoes = false;
    bool clothes = false;
    int x = player->x + 8;
    int y = player->y + player->h - 32;
    int code;
    int codeB;
    int codeC;
    if ( direction == "up" ) { y -= player->speed; }
    else if ( direction == "down" ) { y += player->speed+0.5; }
    else if ( direction == "left" ) { x -= player->speed; }
    else if ( direction == "right" ) { x += player->speed+0.5; }
    for (int i=0; i<8; i++)
    {
        if ( i==0 )
        {
            code = map.bottom_layer[x/32][y/32].codex;
        }
        else if ( i==1 )
        {
            code = map.bottom_layer[x/32+1][y/32].codex;
        }
        else if ( i==2 )
        {
            code = map.bottom_layer[x/32][y/32+1].codex;
        }
        else if ( i==3 )
        {
            code = map.bottom_layer[x/32+1][y/32+1].codex;
        }
        else if ( i==4 )
        {
            code = map.top_layer[x/32][y/32].codex;
        }
        else if ( i==5 )
        {
            code = map.top_layer[x/32+1][y/32].codex;
        }
        else if ( i==6 )
        {
            code = map.top_layer[x/32][y/32+1].codex;
        }
        else if ( i==7 )
        {
            code = map.top_layer[x/32+1][y/32+1].codex;
        }
        if ( i < 4 )
        {
            if ( (code != 416 ) && (code != 448) && (code != 224) && (code != 320) && (code != 128) )
            {
                collide = true;
            }
        }
        else
        {
            if ( (code != 0) && (code != 416 ) && (code != 448) && (code != 224) && (code != 320) && (code != 128) )
            {
                collide = true;
                if ( code == 85*32 || code == 86*32 ) { clothes = true; }
                else if ( code == 93*32 || code == 94*32 ) { makeup = true; }
                else if ( code == 95*32 || code == 96*32 || code == 97*32 ||
                    code == 98*32 || code == 99*32 || code == 100*32 ) { shoes = true; }
                else if ( code == 108*32 || code == 107*32 ) { hair = true; }
            }
        }
    }
    x = player->x;
    y = player->y;
    if ( direction == "up" ) { y -= player->speed; }
    else if ( direction == "down" ) { y += player->speed; }
    else if ( direction == "left" ) { x -= player->speed; }
    else if ( direction == "right" ) { x += player->speed; }
    //check for collision with a clerk
    for (int j=0; j<6; j++)
    {
        if ( clerk[j].map == map_index )
        {
            if (    ( x + player->w     >       clerk[j].x ) &&
                    ( x                 <       clerk[j].x + clerk[j].w ) &&
                    ( y + player->h     >       clerk[j].y ) &&
                    ( y + player->h/2   <       clerk[j].y + clerk[j].h ) )
            {
                //collision

                collide = true;
            }
        }
    }    if ( collide == false )
    {
        if ( direction == "up" )
        {
            player->move("up");
        }
        else if ( direction == "down" )
        {
            player->move("down");
        }
        else if ( direction == "left" )
        {
            player->move("left");
        }
        else if ( direction == "right" )
        {
            player->move("right");
        }
        player->force_update();
    }
    else
    {
        if ( clothes && temp_count == 0.0 ) { player->swap('c'); play_sample(sfx, 255, 128, 1000, false); player->direction="down"; }
        else if ( hair && temp_count == 0.0 ) { player->swap('h'); play_sample(sfx, 255, 128, 1000, false); player->direction="down"; }
        else if ( shoes && temp_count == 0.0) { player->swap('s'); play_sample(sfx, 255, 128, 1000, false); player->direction="down"; }
        else if ( makeup && temp_count == 0.0 ) { player->swap('m'); play_sample(sfx, 255, 128, 1000, false); player->direction="down"; }
    }
}

void init_clerks( rjm_npcClass clerk[6] )
{
    for (int i=0; i<6; i++)
    {
        clerk[i].codex = i*48;
        clerk[i].w = 48;
        clerk[i].h = 96;
    }
    clerk[0].map = 2;           //clothing store
    clerk[0].x = 186;
    clerk[0].y = 122;
    clerk[0].talkx = clerk[0].x;
    clerk[0].talky = clerk[0].y + 96;
    clerk[0].speak[0] = load_sample("shopping_c1_a.wav");
    clerk[0].speak[1] = load_sample("shopping_c1_b.wav");

    clerk[1].map = 3;           //hair salon
    clerk[1].x = 348;
    clerk[1].y = 102;
    clerk[1].talkx = clerk[1].x;
    clerk[1].talky = clerk[1].y + 96;
    clerk[1].speak[0] = load_sample("shopping_c2_a.wav");
    clerk[1].speak[1] = load_sample("shopping_c2_b.wav");

    clerk[2].map = 3;           //hair salon
    clerk[2].x = 566;
    clerk[2].y = 102;
    clerk[2].talkx = clerk[2].x;
    clerk[2].talky = clerk[2].y + 96;
    clerk[2].speak[0] = load_sample("shopping_c4_a.wav");
    clerk[2].speak[1] = load_sample("shopping_c4_b.wav");

    clerk[3].map = 4;           //makeup store
    clerk[3].x = 122;
    clerk[3].y = 196;
    clerk[3].talkx = clerk[3].x;
    clerk[3].talky = clerk[3].y + 96;
    clerk[3].speak[0] = load_sample("shopping_c5_a.wav");
    clerk[3].speak[1] = load_sample("shopping_c5_b.wav");

    clerk[4].map = 4;           //makeup store
    clerk[4].x = 666;
    clerk[4].y = 78;
    clerk[4].talkx = clerk[4].x;
    clerk[4].talky = clerk[4].y + 100;
    clerk[4].speak[0] = load_sample("shopping_c3_a.wav");
    clerk[4].speak[1] = load_sample("shopping_c3_b.wav");

    clerk[5].map = 5;           //shoe store
    clerk[5].x = 212;
    clerk[5].y = 90;
    clerk[5].talkx = 212;
    clerk[5].talky = 90 + clerk[5].h;
    clerk[5].speak[0] = load_sample("shopping_c6_a.wav");
    clerk[5].speak[1] = load_sample("shopping_c6_b.wav");
}









